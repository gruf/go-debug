//go:build !debug && !debugenv
// +build !debug,!debugenv

package debug_test

import (
	"testing"

	"codeberg.org/gruf/go-debug"
)

func TestServePprof(t *testing.T) {
	if debug.ServePprof("") != nil {
		t.Error("expected ServePprof to be noop")
	}
}

func TestWithPprof(t *testing.T) {
	if debug.WithPprof(nil) != nil {
		t.Error("expected WithPprof to be noop")
	}
}
