//go:build !debug && !debugenv
// +build !debug,!debugenv

package debug_test

import (
	"testing"

	"codeberg.org/gruf/go-debug"
)

func TestDebug(t *testing.T) {
	var ok bool
	debug.Run(func() { ok = true })
	if debug.DEBUG || ok {
		t.Fatal("expected debug to be disabled")
	}
}
