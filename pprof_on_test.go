//go:build debug || debugenv
// +build debug debugenv

package debug_test

import (
	"testing"

	"codeberg.org/gruf/go-debug"
)

func TestServePprof(t *testing.T) {
	if debug.DEBUG && debug.ServePprof("") == nil {
		t.Error("expected ServePprof to be enabled")
	} else if !debug.DEBUG && debug.ServePprof("") != nil {
		t.Error("expected ServePprof to be disabled")
	}
}

func TestWithPprof(t *testing.T) {
	if debug.DEBUG && debug.WithPprof(nil) == nil {
		t.Error("expected WithPprof to be enabled")
	} else if !debug.DEBUG && debug.WithPprof(nil) != nil {
		t.Error("expected WithPprof to be disabled")
	}
}
