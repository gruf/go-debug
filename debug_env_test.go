//go:build debugenv
// +build debugenv

package debug_test

import (
	"os"
	"testing"

	"codeberg.org/gruf/go-debug"
)

func TestDebug(t *testing.T) {
	enabled := (os.Getenv("DEBUG") != "")
	var enabledStr string
	if enabled {
		enabledStr = "enabled"
	} else {
		enabledStr = "disabled"
	}

	var ok bool
	debug.Run(func() { ok = true })
	if debug.DEBUG != enabled || ok != enabled {
		t.Fatal("expected debug to be " + enabledStr)
	}
}
